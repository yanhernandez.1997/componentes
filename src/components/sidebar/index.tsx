// @flow 
import * as React from 'react';
import { Button, Card, CardActions, CardContent, makeStyles, Typography } from "@material-ui/core";
type Props = {
    visible: boolean
};

const useStyles = makeStyles({
    root: {
      width: 200,
      height: '100vh',
      position: 'absolute',
      top: 0,
      zIndex: -1,
      paddingTop: 100
    },
    bullet: {
      display: 'inline-block',
      margin: '0 2px',
      transform: 'scale(0.8)',
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
  });
  

export const Sidebar = (props: Props) => {
    const classes = useStyles();
    const bull = <span className={classes.bullet}>•</span>;
    if(!props.visible) return null; 
    return (
        <div>
           <Card className={classes.root}>
                <CardContent>
                   <p>
                   Menu 1
                   </p>
                   <p>
                   Menu 1
                   </p>
                   <p>
                   Menu 1
                   </p>
                   <p>
                   Menu 1
                   </p>
                   <p>
                   Menu 1
                   </p>
                   <p>
                   Menu 1
                   </p>
                </CardContent>
            </Card>
        </div>
    );
};